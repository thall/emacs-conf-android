# emacs-conf-android
Tessa Hall's emacs configuration, for Emacs Android with Termux. See: https://sourceforge.net/projects/android-ports-for-gnu-emacs/files/ and the README there. I should note that both Emacs android, and this config, are in very early stages. TLS may not consistently work within Android GUI emacs, but you can use Termux CLI emacs to work around it.

[Screenshot of Emacs Android open with two windows. A buffer with init.el alongside a dired buffer showing internal android storage.](emacs-android.png)



## Requirements:
- Emacs and Termux apps, built from above SourceForge link in the termux folder

## Optional Dependencies:
- You may want https://github.com/Julow/Unexpected-Keyboard or something like it.
- Nothing else yet...

## Setup:
- Follow intructions to install interoperble Emacs/Termux on sourceforge save for elisp (taken care of here)
- `pkg install git emacs gnutls` in Termux
- copy files into /data/data/org.gnu.emacs/files/.emacs.d/
- In Termux, run `emacs --init-directory /data/data/org.gnu.emacs/files/.emacs.d/` (this will take a while as terminal emacs in Termux fetches remote packages for the first time). Retry if you come across elisp compilation errors unrelated to config.
- Open Emacs Android. It will load the initialized config.

## General Notes:
In all modes something to note is the modeline uses `which-function-mode` to display the name of the function the
cursor is currently within when the programming mode supports it.
Which-key is enabled as well.
Dracula theme.
I set `initial-buffer-choice "/storage/emulated/0"` which requires giving emacs access to all files in the Android System App Info dialogue.


## Special Keybinds:
- `C-x C-f` - `helm-find-files` (this is done globally; so anything invoking `find-file` is fed to `helm-find-files`
- `M-x` - `helm-M-x` let helm handle `M-x` for fuzzy-completion and history
- `M-y` - `helm-show-kill-ring` look through the elements stored in the kill-ring (emacs clipboard, essentially)
- `M-u` - `undo-tree-visualize` handle undos in the traditional emacs fashion, but with a visual aid
- `M-<direction-key>` - resize windows
- `C-h` - `backward-kill-word` deletes the last word behind the cursor (most terminals interpret Ctrl+Backspace as ^C-h and C-Backspace still works by default)
- `M-h` - `help` opens the generic help buffer for emacs (normally `C-h` but this works better in terminal emulators) (`C-h` is still help-suffix when not standalone)
