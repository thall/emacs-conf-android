(setenv "PATH" (format "%s:%s" "/data/data/com.termux/files/usr/bin"
		       (getenv "PATH")))
(push "/data/data/com.termux/files/usr/bin" exec-path)

(when (string-equal system-type "android")
 (setq tls-program '("gnutls-cli -p %p %h"
          "gnutls-cli -p %p %h --protocols ssl3"
          ;"openssl s_client -connect %h:%p -no_ssl2 -ign_eof"
 )))
