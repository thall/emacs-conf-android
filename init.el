;; Tessa Hall's Emacs Android Config

;;;melpa
(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

;; Declare and install packages
(setq my-packages
      '(
				tramp
        helm helm-themes
        magit
        markdown-mode
				which-key
				dracula-theme
				tabspaces
				rainbow-delimiters
				no-littering
				undo-tree
        scala-mode))

(defun my-packages-installed-p ()
  (cl-loop for p in my-packages
	   when (not (package-installed-p p)) do (cl-return nil)
	   finally (cl-return t)))

(unless (my-packages-installed-p)
  ;; check for new packages (package versions)
  (package-refresh-contents)
  ;; install the missing packages
  (dolist (p my-packages)
    (when (not (package-installed-p p))
      (package-install p))))

(message "All done in install-packages.")

;;; mode hooks all listed out for clarity
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
(add-hook 'prog-mode-hook 'hl-line-mode)
(add-hook 'prog-mode-hook (lambda () (setq truncate-lines t)))
(add-hook 'prog-mode-hook 'show-paren-mode)

;;; all global minor-modes to enable/disable on startup
(helm-mode 1)
(electric-pair-mode 1)
(delete-selection-mode 1)
(which-function-mode 1)
(which-key-mode 1)
(tabspaces-mode)
(tool-bar-mode 1)
'(truncate-lines 1)
'(tool-bar-position 'bottom)
'(touch-screen-word-select t)
;; enable below line if you need the modifier keys on touch screen
;; (modifier-bar-mode)

;;; reduce prompts to single letters
(fset 'yes-or-no-p 'y-or-n-p)

;;; default theme
(load-theme 'dracula t)

;;; Keyboard Configuration
;; M-y to cycle through elements in the kill-ring with helm (clipboard)
(global-set-key (kbd "M-y") 'helm-show-kill-ring)
;; M-x to open the helm
(global-set-key (kbd "M-x") 'helm-M-x)
;; M-u to show undo tree
(global-set-key (kbd "M-u") 'undo-tree-visualize)
;; M-h to show generic help (otherwise C-h is the prefix following anything else, still)
(global-set-key (kbd "M-h") 'help)
;; C-h to backward-kill-word (since most terminals interpret Ctrl+Backspace as C-h)
(global-set-key (kbd "C-h") 'backward-kill-word)
;; C-x f to find the definition of what is under the cursor
(global-set-key (kbd "C-x f") 'xref-find-definitions)
;; let helm control the find-file function (C-x f)
(define-key global-map [remap find-file] 'helm-find-files)
;; window controls:
(global-set-key (kbd "<M-up>") 'shrink-window)
(global-set-key (kbd "<M-down>") 'enlarge-window)
(global-set-key (kbd "<M-left>") 'shrink-window-horizontally)
(global-set-key (kbd "<M-right>") 'enlarge-window-horizontally)

;; tab settings:
'(tab-bar-close-last-tab-choice 'delete-frame)
'(tab-bar-mode t)
'(tab-bar-position t)
'(tab-bar-select-tab-modifiers '(meta))
'(tab-bar-tab-hints t)
'(tab-bar-tab-name-function 'tab-bar-tab-name-current-with-count)
'(tab-bar-tab-name-truncated-max 30)

;;; miscellanious settings
;; Keep auto-save/backup files separate from source code:  https://github.com/scalameta/metals/issues/1027
(setq-default auto-mode-alist (cons '("\\.scala$" . scala-mode) auto-mode-alist)
	      ediff-window-setup-function 'ediff-setup-windows-plain
	      x-select-enable-clipboard-manager nil
	      helm-home-url "https://www.duckduckgo.com"
	      tab-width 2 ;; indentation needs to be less intense these days, jeez
	      initial-buffer-choice "/storage/emulated/0" ;; just my preference
	      ring-bell-function 'ignore
	      show-trailing-whitespace t
	      indicate-empty-lines t
	      show-paren-delay 0
	      backup-by-copying t
	      sentence-end-double-space nil
	      tab-always-indent 'complete
	      eshell-buffer-shorthand t
	      backup-directory-alist `((".*" . ,temporary-file-directory))
	      auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))

;;; eshell customization
(require 'eshell)
;; alias notes:
;; top aliased to proced
;; diff aliased to ediff-files
;; check eshell/alias for more
(defvar eshell-visual-commands
  '("vi" "vim"										      ; what is going on??
    "screen"                            ; ok, a valid program...
    "less" "more"                       ; M-x view-file
    "lynx" "ncftp"                      ; w3.el, ange-ftp
    "pine" "tin" "trn" "elm"						; GNUS!!
    )           )
;; I use proced instead of top: here are some good defaults
(require 'proced)
(proced-toggle-auto-update 1)
(setq proced-auto-update-interval 1)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
	 '(dracula-theme shades-of-purple-theme helm helm-themes magit markdown-mode no-littering rainbow-delimiters scala-mode tabspaces undo-tree which-key)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
